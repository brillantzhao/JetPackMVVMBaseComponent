package com.zzk.baselibrary.base

import android.app.Application
import com.alibaba.android.arouter.launcher.ARouter
import com.tencent.mmkv.MMKV
import com.zzk.baselibrary.R
import com.zzk.jpmvvmbase.base.BaseApplication

/**
 * 添加该类的目的是在最精简BaseApplication的基础上封装一些和应用相关的初始化工作，最主要是满足组件化的需求，
 * 如果没有组件化的需要的话，可以去掉该层封装，直接放在最终的MyApplication中
 *
 * @ProjectName:    JetPackMVVMBaseComponent
 * @Package:        com.zzk.baselibrary.base
 * @ClassName:      JPBaseApplication
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.21 8:13
 * @UpdateUser:
 * @UpdateDate:     2021.1.21 8:13
 * @UpdateRemark:
 * @Version:        1.0.0
 */
open class JPBaseApplication : BaseApplication() {

    //##########################  custom variables start ##########################################

    var isOnlineVersion = false
    var isShowLog = false

    companion object {
        lateinit var instance: JPBaseApplication
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun onCreate() {
        super.onCreate()
        instance = this
        //
        initGlobalInfo()
        // 调用父类的初始化方法
        initUtilCodeXLog(isShowLog)
        //
        initARouter(isOnlineVersion, this)
        // 初始化 MMKV，设定 MMKV 的根目录（files/mmkv/）
        MMKV.initialize(this.filesDir.absolutePath + "/mmkv")
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     *
     */
    private fun initARouter(isOnlineVersion: Boolean, application: Application) {
        //=== 初始化阿里路由框架
        if (isOnlineVersion) {    // 这两行必须写在init之前，否则这些配置在init过程中将无效
            ARouter.openLog()     // 打印日志
            ARouter.openDebug()   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        }
        ARouter.init(application) // 尽可能早，推荐在Application中初始化
    }

    /**
     *
     */
    private fun initGlobalInfo() {
        // 获取app当前是否是ONLINE状态，即正式的线上发布状态，此时需要关闭日志
        isOnlineVersion = resources.getBoolean(R.bool.isOnlineVersion)
        // 判断日志是否展示
        isShowLog = resources.getBoolean(R.bool.isShowLog)
    }

    //##########################  custom metohds end   ############################################

}