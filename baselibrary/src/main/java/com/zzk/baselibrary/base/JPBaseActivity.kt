package com.zzk.baselibrary.base

import androidx.databinding.ViewDataBinding
import com.zzk.jpmvvmbase.base.BaseActivity
import com.zzk.jpmvvmbase.base.BaseViewModel

/**
 *
 * @ProjectName:    JetPackMVVMBaseComponent
 * @Package:        com.zzk.baselibrary.base
 * @ClassName:      JPBaseActivity
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.21 8:47
 * @UpdateUser:
 * @UpdateDate:     2021.1.21 8:47
 * @UpdateRemark:
 * @Version:        1.0.0
 */
abstract class JPBaseActivity<VM : BaseViewModel, DB : ViewDataBinding> : BaseActivity<VM, DB>() {

    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun showLoading(message: String) {
    }

    override fun dismissLoading() {
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}