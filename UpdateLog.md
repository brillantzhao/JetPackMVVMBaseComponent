## 更新日志

## v1.0.0

**2021年1月14日**

- 全面升级AAC，引入谷歌lifecycle组件；
- 优化框架代码，解决已知Bug；
- 全面升级RxJava3；
- 新增ViewPager+Fragment例子；
- 新增RecycleView多布局例子；
- 修改例子程序；
- 升级第三方依赖库；
- 修改文档说明。