package com.zzk.jpcomponent

import cat.ereza.customactivityoncrash.config.CaocConfig
import com.kingja.loadsir.callback.SuccessCallback
import com.kingja.loadsir.core.LoadSir
import com.tencent.bugly.Bugly
import com.zzk.baselibrary.base.JPBaseApplication
import com.zzk.baselibrary.widget.loadCallBack.EmptyCallback
import com.zzk.baselibrary.widget.loadCallBack.ErrorCallback
import com.zzk.baselibrary.widget.loadCallBack.LoadingCallback

/**
 *
 * @ProjectName:    JetPackMVVMBaseComponent
 * @Package:        com.zzk.jpcomponent
 * @ClassName:      MyApplication
 * @Description:
 * @Author:         brilliantzhao
 * @CreateDate:     2021.1.21 8:29
 * @UpdateUser:
 * @UpdateDate:     2021.1.21 8:29
 * @UpdateRemark:
 * @Version:        1.0.0
 */

class MyApplication : JPBaseApplication() {

    //##########################  custom variables start ##########################################

    companion object {
        lateinit var instance: MyApplication
    }

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    override fun onCreate() {
        super.onCreate()
        instance = this
        //
        initCaocConfig()
        //界面加载管理 初始化
        initLoadSir()
        //初始化Bugly
        initBugly()
    }

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    /**
     *
     */
    private fun initCaocConfig() {
        //防止项目崩溃，崩溃后打开错误界面
        CaocConfig.Builder.create()
            .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT) //default: CaocConfig.BACKGROUND_MODE_SHOW_CUSTOM
            .enabled(true)//是否启用CustomActivityOnCrash崩溃拦截机制 必须启用！不然集成这个库干啥？？？
            .showErrorDetails(true) //是否必须显示包含错误详细信息的按钮 default: true
            .showRestartButton(true) //是否必须显示“重新启动应用程序”按钮或“关闭应用程序”按钮default: true
            .logErrorOnRestart(true) //是否必须重新堆栈堆栈跟踪 default: true
            .trackActivities(true) //是否必须跟踪用户访问的活动及其生命周期调用 default: false
            .minTimeBetweenCrashesMs(2000) //应用程序崩溃之间必须经过的时间 default: 3000
            .errorDrawable(R.mipmap.ic_launcher) //错误图标
//            .restartActivity(SplashActivity::class.java) // 重启的activity
//            .errorActivity(ErrorActivity::class.java) //发生错误跳转的activity
            //                .eventListener(new YourCustomEventListener()) //崩溃后的错误监听
            .apply()
    }

    /**
     *
     */
    private fun initLoadSir() {
        LoadSir.beginBuilder()
            .addCallback(LoadingCallback())//加载
            .addCallback(ErrorCallback())//错误
            .addCallback(EmptyCallback())//空
            .setDefaultCallback(SuccessCallback::class.java)//设置默认加载状态页
            .commit()
    }

    /**
     *
     */
    private fun initBugly() {
        /**
         * 统一初始化方法：
         * 如果您之前使用过Bugly SDK，请将以下这句注释掉。
        CrashReport.initCrashReport(getApplicationContext(), “注册时申请的APPID”, false);
         */
        /**
        第三个参数为SDK调试模式开关，调试模式的行为特性如下：
        输出详细的Bugly SDK的Log；
        每一条Crash都会被立即上报；
        自定义日志将会在Logcat中输出。
        建议在测试阶段建议设置成true，发布时设置为false。
         */
        Bugly.init(applicationContext, "注册时申请的APPID", !isOnlineVersion)
    }

    //##########################  custom metohds end   ############################################


    //##########################  custom variables start ##########################################

    //##########################  custom variables end  ###########################################

    //##########################  override custom metohds start ###################################

    //##########################  override custom metohds end  ####################################

    //##########################  override third methods start ####################################

    //##########################  override third methods end  #####################################

    //##########################  custom metohds start     ########################################

    //##########################  custom metohds end   ############################################

}